/* Copyright (c) 2023 Roland Mandzolo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */


import van from "vanjs-core"

const {header, footer, main, h1, h2, p, div, pre, button, br} = van.tags
const {use, svg} = van.tagsNS("http://www.w3.org/2000/svg")


//Game logic
const data = [
  { text: "sad", id: "sad" },
  { text: "angry", id: "angry" },
  { text: "happy", id: "happy" },
  { text: "medicine", id: "medicine" },
  { text: "medical-kit", id: "medical-kit" },
  { text: "pulse", id: "pulse" },
  { text: "heptagon", id: "heptagon" },
  { text: "hexagon", id: "hexagon" },
  { text: "pentagon", id: "pentagon" },
  { text: "female", id: "female" },
  { text: "handicap", id: "handicap" },
  { text: "male", id: "male" },
  { text: "mute", id: "mute" },
  { text: "pause", id: "pause" },
  { text: "play", id: "play" },
  { text: "power-button", id: "power-button" },
  { text: "wifi", id: "wifi" },
  { text: "cold", id: "cold" },
  { text: "warm", id: "warm" },
  { text: "rainy", id: "rainy" },
  { text: "secure", id: "secure" },
  { text: "unlock", id: "unlock" },
  { text: "chat", id: "chat" },
  { text: "rss", id: "rss" },
  { text: "messages", id: "messages" },
  { text: "credit-card", id: "credit-card" },
  { text: "euro", id: "euro" },
  { text: "bill", id: "bill" },
  { text: "share", id: "share" },
  { text: "unsecure", id: "unsecure" }
];

let isGameStarted = van.state(false);

let gameOver = van.state('none');

let startButton = van.state('');

let iconsWrapper = van.state('none');

let gameTime = van.state('none');

let gamePoints = van.state(0);

let gameCounter = van.state(0);

let gameDataId = van.state('');

let gameDataText = van.state('Are you ready to have fun');

const getDataGameEnemyText = (data: any[]) => {
  if (isGameStarted.val) {
    gameDataId.val = data[(Math.floor(Math.random() * data.length))].id
    return data.find(item => item.id === gameDataId.val).text
  }
}

const startGame = (state: string) => {
  isGameStarted.val = true;

  resetIcons();

  start();

  if (state === 'isStartButton') {
    startButton.val = "none";
  }

  if (state === 'isIconsWrapper') {
   iconsWrapper.val = "";
  }

  if (state === 'isGameTime') {
    gameTime.val = "";
  }
}

const addStyleToStartButton = () => {
  return `display: ${van.val(startButton) ? "none" : null};`
}

const addStyleToIconsWrapper = () => {
  return `display: ${van.val(startButton) ? null : "none"};`
}

const addStyleToGameTime = () => {
  return `display: ${van.val(startButton) ? null : "none"};`
}

const addStyleToGamePoints = () => {
  return `display: ${van.val(startButton) ? null : "none"};`
}
const addStyleToGameOver = () => {
  return `display: ${van.val(gameOver) ? "none" : "block"};`
}
const addStyleToGameOverIcons = () => {
  return `display: ${van.val(gameOver) ? null : "none"};`
}

const gameStart = () => div({class: "game-button-box", style: addStyleToStartButton()},
  button({class: "game-button", onclick: () => startGame('isStartButton')}, "Start the game")
)

const resetIcons = () => {
  gameDataText.val = getDataGameEnemyText(data);
}

//Webworker

//Sprite logic
const sprite = (category: string, iconId: any) => {
  return button(
    {class: "icon-button", onclick: () => check(iconId)},
    svg(
    {viewBox:"0 0 24 24", style: () => `font-size: 1em; width: 1em; height: 1em;};`},
    use({href:`assets/icons/${category}/${category}-sprite.svg#${iconId}`})
  ))
}

const renderGameOver = () => {
  return div({class: "game-over", style: addStyleToGameOver()}, "(✖╭╮✖)")
}

//Icons logic
const check = (iconId: string)  => { 
  if (gameCounter.val === 10) {
    clearInterval(timeId), timeId = 0
    gameOver.val = '';
    iconsWrapper.val = 'none';
    gameDataText.val = "Game over";
    return
  }
  if (iconId !== gameDataId.val) {
    resetIcons();
    ++gameCounter.val;
  } else {
   ++gamePoints.val;
   resetIcons();
   ++gameCounter.val;
  }
}

const getRandomIcon = (arr: string[]) => {
    if (arr.includes(gameDataId.val)) {

      return arr.find(item => item === gameDataId.val);
    } else {
      
    const randomIndex = Math.floor(Math.random() * arr.length);

    return arr[randomIndex];
  }
}

const getIconData = (id: string) => { 
  if (id === undefined) {
      return
  } 
  const iconData = [
  { id: 'emoji', iconNames: ["angry", "happy", "sad"] },
  { id: 'medical', iconNames: ["medical-kit", "medicine", "pulse"] },
  { id: 'symbol', iconNames: ["female", "handicap", "male"] },
  { id: 'business', iconNames: ["bill", "credit-card", "euro"] },
  { id: 'communication', iconNames: ["chat", "messages", "rss"] },
  { id: 'multimedia', iconNames: ["mute", "pause", "play"] },
  { id: 'security', iconNames: ["secure", "unlock", "unsecure"] },
  { id: 'shapes', iconNames: ["heptagon", "hexagon", "pentagon"] },
  { id: 'user-interface', iconNames: ["power-button", "wifi", "share"] },
  { id: 'weather', iconNames: ["cold", "rainy", "warm"] },
]
  return iconData.find((item => item.id === id))?.iconNames;
}

const icons = () => {
  const listIcons = div(
    {class: "icons-wrapper", style: addStyleToGameOverIcons()},
    sprite('emoji', getRandomIcon(getIconData('emoji') as string[])),
    sprite('medical', getRandomIcon(getIconData('medical') as string[])),
    sprite('symbol', getRandomIcon(getIconData('symbol') as string[])),
    sprite('business', getRandomIcon(getIconData('business') as string[])),
    sprite('communication', getRandomIcon(getIconData('communication') as string[])),
    sprite('multimedia', getRandomIcon(getIconData('multimedia') as string[])),
    sprite('security', getRandomIcon(getIconData('security') as string[])),
    sprite('shapes', getRandomIcon(getIconData('shapes') as string[])),
    sprite('user-interface', getRandomIcon(getIconData('user-interface') as string[])),
    sprite('weather', getRandomIcon(getIconData('weather') as string[])),
  )

  return div(
    {class: "icons-box", style: addStyleToIconsWrapper()},
    listIcons,
    renderGameOver
  )
}

//time logic
let timeId: any
const start = () => timeId = timeId || setInterval(() => elapsed.val += .01, 10)
const elapsed = van.state(0)

const gamePointsScore = () => {
  return div({class: "game-points", style: addStyleToGamePoints()},
    pre("Points:", () => gamePoints.val, "/10")
  )
}

const stopwatch = () => {
  return div({class: "game-time", style: addStyleToGameTime()},
    pre(() => elapsed.val.toFixed(2), "s ")
  )
}

//Header content
const headerData = () => header(
  div(
    {class: "container"},
    br(),
    h1("Iconify")
  )
)


//Game content
const mainData = () => main(
  div(
      {class: "container"},
      div({
        class: "game game--head"},
        div({class: "icon-placeholder"}, "?"),
        div({
        class: "game-enemy"}, 
        div({class: "game-enemy__text", dataGameAttrContent: gameDataText}
        ))),
    
    div({
      class: "game game--body"},
      gameStart,
      icons,
      div({
      class: "game-status"},
      stopwatch,
      gamePointsScore)
    ),
    div(
      {class: "game-description"},
      h2({class: "game-description__heading"},"How to Play?"),
      p("Choose your icon to match the given word or quote."),
      p("As fast as you can and beat your own record and earn reward points.") 
     )
  )
)


//Footer content
const footerData = () => footer(
  div(
    {class: "container"},
    br(),
    p("© Rolaman 2023")
  )
)

//key navigation


van.add(document.body, headerData(), mainData(), footerData())
